# myrc.sh
MYRC_DIR=$( dirname $0 )

source $MYRC_DIR/init/colors.sh

# See init/colors.sh
GIT_DIRTY=$(fg_color "214")
GIT_CLEAN=$(fg_color "190")

# This will just execute EVERYTHING in the lib folder
for libs in $MYRC_DIR/lib/*.sh; do
	source "$libs" || break
done

. $MYRC_DIR/prompt.sh
