# Assert if the branch is dirty or not.
parse_git_dirty() {
    local p_color
    if [[ $(git status 2> /dev/null | tail -n1) != "nothing to commit, working tree clean" ]]; then
	    p_color=$2
    else 
	    p_color=$1
    fi

    if [ ! -z "$3" ]; then
        local branch=$(command git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
        if [ -z "$branch" ]; then
            p_color=$3
        fi
    fi
    echo "$p_color"
}

git_info() {
	# Assert if we're in a Git repository
	local branch=$(command git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! -z $branch ]; then
		echo "$branch"
	else
		echo 
	fi
}