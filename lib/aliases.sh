
alias gc="git commit"
alias gst="git status"
alias gp="git push"
alias gb="git branch"
alias gco="git checkout"
alias gs="git stash"

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."

alias ll="ls -la"
alias vscode="/Applications/Visual\ Studio\ Code.app/Contents/MacOS/Electron"
