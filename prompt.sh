# This file might need some cleanup

update_ps1() {

    # We avoid using PS vars like \h \w, as they cannot be expanded by
    # anything but PS. I.e. we cannot calculate a correct length
    P_USER=$(whoami)
    P_HOST=$(hostname)
    P_DIR=${PWD##*/}
    P_GIT=$(git_info)

    P_GIT_COLOR=$(parse_git_dirty "$GIT_CLEAN" "$GIT_DIRTY" "$FG_BLUE")
    #PS_PLAIN="$P_USER@$P_HOST:$P_DIR $P_GIT"
    if [ ! -z $P_GIT ]; then
        PS_PLAIN="$P_DIR [$P_GIT]"
        PS_LEFT="$FG_BLUE$P_DIR [$P_GIT_COLOR$P_GIT$FG_RESET$FG_BLUE] $COL_RESET"
    else
        PS_PLAIN="$P_DIR"
        PS_LEFT="$FG_BLUE$P_DIR $COL_RESET"
    fi
    #PS_PLAIN="$P_DIR $P_GIT"

    # PS_LEFT="$P_USER@$P_HOST:$FG_BLUE$P_DIR$FG_RESET $FG_BLUE$COL_RESET"
    #PS_RIGHT="${P_GIT_COLOR}$P_GIT${COL_RESET}"

    PS_LEN=$(($COLUMNS - ${#PS_PLAIN}))
    echo $PS_LEFT
    printf "%-${PS_LEN}s"
    #echo $PS_RIGHT
    # PS1="$PS_LEFT$ "
}
PS1="$ "
#PROMPT_COMMAND='update_ps1'
precmd() { update_ps1 }
