# Setup
Initialize the script from an rc file.

For instance add the following, to `.bashrc`

```bash
source /Users/jeppech/.myrc/init.sh
```

# Customization
For now, make your changes to the `prompt.sh`